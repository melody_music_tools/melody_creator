use std::fs;

use super::BookConfig;

pub fn get_all_available_configuration_files() -> std::io::Result<Vec<BookConfig>> {
    let directory_contents = fs::read_dir(".").unwrap();

    let mut configurations = Vec::new();

    for possible_directory_item in directory_contents {
        let directory_item = possible_directory_item.unwrap();
        if directory_item.file_name().to_str().unwrap().contains(".melody.toml") {
            configurations.push(
                toml::from_str(&fs::read_to_string(directory_item.path()).unwrap())
                    .unwrap());
        }
    }

    return Ok(configurations);
}
