use serde_derive::Deserialize;
use crate::book_data::BookData;
use crate::build_config::BuildConfig;

#[derive(Debug, Deserialize)]
pub struct BookConfig {
    pub data: BookData,
    pub build: BuildConfig,
}

impl BookConfig {
    pub fn generate_book_name(&self) -> String {
        self.data.title.to_ascii_lowercase().replace(" ", "_") +
            "_" +
            &self.data.copyright.year.to_string()
    }
}

