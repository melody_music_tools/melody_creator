use getset::Getters;
use serde_derive::{Deserialize, Serialize};

use super::MMetaAuthor;

#[derive(Clone, Debug, Getters, Deserialize, Serialize)]
pub struct MMetaLyrics {
    #[getset(get = "pub")]
    title: String,
    #[getset(get = "pub")]
    authors: Vec<MMetaAuthor>,
}
