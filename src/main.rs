use std::fs::File;
use std::process::exit;

use clap::{Arg, ArgMatches, command};
use console::style;
use log::info;
use simplelog::{LevelFilter, WriteLogger};

mod book_components;

mod configuration;
use configuration::BookConfig;
use configuration::utils::get_all_available_configuration_files;

mod book_data;
use book_data::BookData;

mod book_file_data;

mod book_structure;
use book_structure::BookStructure;

mod build_config;

mod metadata;

mod subcommands;

fn get_args() -> ArgMatches {
    return command!()
        .propagate_version(true)
        .about("Tool to build and manipulate music book files")
        .subcommand_required(true)
        .arg(Arg::new("config-file")
            .long("config-file")
            .short('c')
            .global(true))
        .subcommand(clap::Command::new("clean")
            .about("Clean up build files"))
        .subcommand(clap::Command::new("generate")
            .about("Generate new music book file"))
        .subcommand(clap::Command::new("login")
            .about("Login to remote Melody server"))
        .subcommand(clap::Command::new("upload")
            .about("Upload a generated book to a server"))
        .get_matches();
}

fn process_with_configuration(args: &ArgMatches, configuration: &BookConfig) {
    match args.subcommand() {
        Some(("clean", _subcommand_args)) => subcommands::clean(configuration),
        Some(("generate", subcommand_args)) => subcommands::generate_from_configuration(configuration, subcommand_args),
        Some(("login", _subcommand_args)) => subcommands::login(configuration.build.servers.first().unwrap()),
        Some(("upload", _subcommand_args)) => subcommands::upload_from_configuration(configuration),
        _ => unreachable!("subcommand")
    }
}

fn main() {
    let args = get_args();

    let log_file = File::create("melody_creator.log").unwrap();
    WriteLogger::init(LevelFilter::Info, simplelog::Config::default(), log_file).unwrap();

    println!("Searching for configurations...");
    let available_configurations = get_all_available_configuration_files().unwrap();

    if available_configurations.len() > 0 {
        println!("Found configurations for the following books:");
        for available_configuration in &available_configurations {
            println!("    {}", available_configuration.data.title);
        }
    } else {
        println!("No configurations found, exiting.");
        info!("No configurations found, exiting.");
        exit(0);
    }

    println!("Processing configurations...");
    for (index, configuration) in available_configurations.iter().enumerate() {
        println!("{} {}, edition: {}",
                 style(format!("Book {} of {}", index + 1, available_configurations.len())).bold(),
                 configuration.data.title,
                 configuration.data.edition);
        process_with_configuration(&args, configuration);
    }
}