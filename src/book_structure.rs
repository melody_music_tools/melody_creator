use derive_new::new;
use getset::Getters;
use serde_derive::Serialize;

#[derive(Debug, Getters, Serialize, new)]
pub struct BookStructure {
    #[getset(get = "pub")]
    file_names: Vec<String>,
}
