use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Publisher {
    pub name: String,
    pub location: String,
}
