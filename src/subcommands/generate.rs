use std::fs;
use std::path::PathBuf;
use std::process::Command;
use std::str;

use chrono::{Datelike, Utc};
use clap::ArgMatches;
use indicatif::ParallelProgressIterator;
use log::info;
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};

use crate::book_file_data::BookFileData;
use crate::book_structure::BookStructure;
use crate::configuration::BookConfig;
use crate::metadata::MMeta;

pub fn generate_from_configuration(configuration: &BookConfig, _args: &ArgMatches) {
    let input_path = &configuration.build.source_directory;
    let export_dir = configuration.generate_book_name();

    let current_year = Utc::now().year() as u32;

    let mut files_to_export = Vec::new();
    for metadata_file in get_metadata_files(configuration) {
        let metadata: MMeta = serde_yaml::from_str(fs::read_to_string(metadata_file).unwrap().as_str()).unwrap();

        let years_since_most_recent_author_death = metadata.lyrics().authors()
            .iter()
            .map(|author| current_year - author.death_year().unwrap_or(current_year))
            .chain(
                metadata.music().authors()
                    .iter()
                    .map(|author| current_year - author.death_year().unwrap_or(current_year)))
            .min();

        match years_since_most_recent_author_death {
            Some(years) => {
                if years > 70 {
                    let mut path = PathBuf::from(input_path);
                    path.push(metadata.associated_file());
                    files_to_export.push(path);
                } else {
                    println!("File is still in copyright");
                }
            },
            None => {
                let mut path = PathBuf::from(input_path);
                path.push(metadata.associated_file());
                files_to_export.push(path);
            }
        }
    }

    fs::create_dir(&export_dir).unwrap();

    println!("    Generating musicXML files from source...");
    files_to_export.par_iter().progress().for_each(|file_path| {
        generate_musicxml(
            file_path.to_str().unwrap(),
            &format!("{}/{}.musicxml",
                     &export_dir,
                     file_path.file_stem().unwrap().to_string_lossy().to_string())
        );
    });

    let mut book_config_file_names = files_to_export
        .into_iter()
        .map(|file| format!("{}/{}", &export_dir, file.file_name().unwrap().to_string_lossy().to_string().replace(".mscz", ".musicxml")))
        .collect::<Vec<String>>();
    book_config_file_names.sort();
    let book_config = BookFileData::new(
        configuration.data.clone(),
        BookStructure::new(book_config_file_names));
    let book_config_json_string = serde_json::to_string(&book_config).unwrap();

    fs::write(format!("{}.json", &export_dir), book_config_json_string).unwrap();

    println!("    Done! Book files were written to directory {}", export_dir);
}

fn get_metadata_files(configuration: &BookConfig) -> Vec<PathBuf> {
    let input_path = &configuration.build.source_directory;

    let mut files_to_export = Vec::new();
    for dir_entry in fs::read_dir(input_path).unwrap() {
        let file = dir_entry.unwrap();
        if let Some(extension) = file.path().extension() {
            if extension.to_str().unwrap() == "mmeta" {
                files_to_export.push(file.path());
            }
        }
    }

    return files_to_export;
}

fn generate_musicxml(input_path: &str, export_path: &str) {
    let mut command= Command::new("mscore");
    command.env("QT_QPA_PLATFORM", "offscreen");
    command.arg(input_path);
    command.arg("--export-to");
    command.arg(export_path);
    let command_output = command
        .output()
        .expect("failed to run musescore");

    let mut command_string = command.get_program().to_string_lossy().to_string();
    for arg in command.get_args() {
        command_string.push_str(arg.to_str().unwrap());
    }
    info!(
        "stdout for command:\n{}\nis\n{}",
        command_string,
        str::from_utf8(&command_output.stdout).unwrap()
    );
}
