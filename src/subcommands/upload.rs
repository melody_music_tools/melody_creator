use std::fs;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use reqwest::blocking::Client;
use reqwest::blocking::multipart::{Form, Part};
use reqwest::header::AUTHORIZATION;
use reqwest::{StatusCode, Url};

use crate::BookConfig;

pub fn upload_from_configuration(configuration: &BookConfig) {
    let book_name = configuration.generate_book_name();
    let http_client = reqwest::blocking::Client::new();
    println!("Uploading files from: {}", book_name);
    for entry in fs::read_dir(&book_name).unwrap() {
        let file = entry.unwrap();
        let mut file_to_send = File::open(file.path()).unwrap();
        let mut content_to_send = Vec::new();
        if file_to_send.read_to_end(&mut content_to_send).unwrap() == 0 {
            panic!("file has no content");
        }
        for server in &configuration.build.servers {
            upload_file(&http_client, server, &file.path());
        }
    }
    for server in &configuration.build.servers {
        upload_file(&http_client, server, &PathBuf::from(format!("{}.json", book_name)));
    }
}

fn upload_file(http_client: &Client, server: &String, file: &PathBuf) {
    let uploaded_path = file.to_str().unwrap();
    println!("    Uploading {} to server: {}", file.file_name().unwrap().to_str().unwrap(), server);
    let part = Part::file(&uploaded_path).unwrap();
    let form = Form::new().part("file", part);
    let full_url = Url::parse(server).unwrap()
        .join("restricted/upload/").unwrap()
        .join(&uploaded_path).unwrap();
    println!("Full URL is: {}", full_url.as_str());
    let jwt = get_jwt();
    let response = http_client.post(full_url)
        .header(AUTHORIZATION, format!("Bearer {jwt}"))
        .multipart(form)
        .send();

    let message = match response {
        Ok(response_content) => match response_content.status() {
            StatusCode::ACCEPTED => "File successfully uploaded".to_string(),
            _ => format!(
                "Upload failed with error from server:\n        code: {:?}\n        message: {}",
                response_content.status(),
                response_content.text().unwrap_or("".to_string()))
        },
        Err(error) => format!("Upload failed with error: {:?}", error),
    };

    println!("    {}", message);
}

fn get_jwt() -> String {
    let mut dir = dirs::cache_dir().unwrap();
    dir.push("melody");
    dir.push("creator");
    fs::create_dir_all(&dir).unwrap();
    let mut path = dir.clone();
    path.push("token");
    fs::read_to_string(path).unwrap()
}
