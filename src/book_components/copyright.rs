use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Copyright {
    pub year: u32,
    pub month: u8,
}
