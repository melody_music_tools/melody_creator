use std::thread::sleep;
use std::time::Duration;

use reqwest::header::ACCEPT;
use serde_derive::Deserialize;
use serde_json::Value;
use reqwest::Url;

#[derive(Debug, Deserialize)]
struct LoginResponse {
    device_code: String,
    user_code: String,
    verification_uri: String,
    expires_in: u64,
    interval: u64,
}

pub fn login(melody_base_url: &str)
{
    let mut melody_login_url = Url::parse(melody_base_url).unwrap();
    melody_login_url.set_path("login");
    let response = reqwest::blocking::get(melody_login_url).unwrap();
    let login_response = serde_json::from_slice::<LoginResponse>(response.bytes().unwrap().as_ref()).unwrap();
    open::that(login_response.verification_uri).unwrap();
    println!(
        "Your login code is {}, and expires in {}",
        login_response.user_code,
        login_response.expires_in);

    let client = reqwest::blocking::Client::new();
    let mut authenticated = false;
    let base_url = "https://github.com/login/oauth/access_token";
    let login_params = vec![
        ("client_id", "23319ec20e31400a9e45"),
        ("device_code", login_response.device_code.as_str()),
        ("grant_type", "urn:ietf:params:oauth:grant-type:device_code")];
    let full_url = Url::parse_with_params(base_url, login_params).unwrap();
    while !authenticated {
        sleep(Duration::from_secs(login_response.interval));
        let check_response = client
            .post(full_url.clone())
            .header(ACCEPT, "application/json")
            //.header(USER_AGENT, "me")
            .send()
            .unwrap();
        //println!("{:?}", check_response);
        //println!("{:?}", String::from_utf8(check_response.bytes().unwrap().to_vec()));
        let output = serde_json::from_slice::<Value>(&check_response.bytes().unwrap().clone()).unwrap();
        let o = output.as_object().unwrap();
        if o.contains_key("access_token") {
            authenticated = true;
            let token = o.get("access_token").unwrap().as_str().unwrap().clone();
            let auth_params = vec![("code", token)];
            let mut melody_auth_url = Url::parse_with_params(melody_base_url, auth_params).unwrap();
            melody_auth_url.set_path("auth");
            let r = reqwest::blocking::get(melody_auth_url).unwrap();
            let jwt = String::from_utf8(r.bytes().unwrap().to_vec()).unwrap();
            save_jwt(jwt);
        }
    }
}

fn save_jwt(jwt: String) {
    let mut dir = dirs::cache_dir().unwrap();
    dir.push("melody");
    dir.push("creator");
    std::fs::create_dir_all(&dir).unwrap();
    let mut path = dir.clone();
    path.push("token");
    std::fs::write(path, jwt).unwrap();
}