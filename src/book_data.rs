use serde_derive::{Deserialize, Serialize};
use super::book_components::{Copyright, Publisher};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct BookData {
    pub title: String,
    pub subtitles: Vec<String>,
    pub edition: String,
    pub printer: String,
    pub prefaces: Vec<String>,
    pub acknowledgements: Vec<String>,
    pub copyright: Copyright,
    pub publisher: Publisher,
}
