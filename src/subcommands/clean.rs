use std::fs;

use crate::BookConfig;

pub fn clean(configuration: &BookConfig) {
    println!("Cleaning...");
    let name = configuration.generate_book_name();
    let directory_contents = fs::read_dir(".").unwrap();

    for possible_directory_item in directory_contents {
        let directory_item = possible_directory_item.unwrap();
        let directory_item_name = directory_item.file_name();
        if directory_item_name.to_str().unwrap() == name {
            println!(
                "    Deleting temporary directory: {}",
                directory_item.path().to_str().unwrap());
            fs::remove_dir_all(directory_item.path()).unwrap();
        } else if directory_item_name.to_str().unwrap() == format!("{}.json", name) {
            println!(
                "    Deleting book manifest: {}",
                directory_item.path().to_str().unwrap());
            fs::remove_file(directory_item.path()).unwrap();
        }
    }
    println!("Done");
}

